# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  arr = str.chars.select! { |ch| ch == ch.upcase }
  arr.join
end

puts

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  l = str.length
  half = l / 2

  result =
    if l % 2 == 0
      str[(half-1)..half]
    else
      str[half]
    end

  return result
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  arr = str.chars.select { |l| VOWELS.include?(l) }
  arr.length
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  product = 1
  (1..num).each { |n| product *= n }
  product
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")

  new_str = String.new
  arr.each { |item| new_str += "#{item}#{separator}" }

  result =
    if arr.empty?
      ""
    elsif separator == ""
      new_str
    else
      new_str.chop
    end

  result
end


# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  new_str = String.new
  str.chars.each_with_index do |ch, idx|
    if idx % 2 == 0
      new_str += ch.downcase
    else
      new_str += ch.upcase
    end
  end
  new_str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  word_arr = str.split(/\s+/)
  word_arr.map! do |word|
    if word.length >= 5
      word.reverse
    else
      word
    end
  end
  word_arr.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  no_fizz_arr = (1..n).to_a

  fizz_arr = no_fizz_arr.map! do |item|
    if (item % 3 == 0) && (item % 5 == 0)
      "fizzbuzz"
    elsif item % 3 == 0
      "fizz"
    elsif item % 5 == 0
      "buzz"
    else
      item
    end
  end

  fizz_arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  result =
    if num <= 1
      false
    elsif num == 2
      true
    else
      (2...num).to_a.all? { |divisor| num % divisor != 0 }
    end

  result
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factor_arr = Array.new
  (1..num).each { |n| factor_arr << n if num % n == 0 }
  factor_arr
end


# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factor_arr = factors(num)
  factor_arr.select { |n| prime?(n) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odds = Array.new
  evens = Array.new

  arr.each do |num|
    if num % 2 == 0
      evens << num
      return odds[0] if evens.length > 1 && odds.length > 0
    else
      odds << num
      return evens[0] if odds.length > 1 && evens.length > 0
    end
  end

end
